#!/bin/bash

set -euo pipefail

declare user_install_location="${1:-/usr/local/bin}"

install_latest() {
    local tmp_dir=''
    tmp_dir=$(mktemp -d)

    local dist_url=''
    local install_location="$1"

    dist_url="$(
        curl -s https://gitlab.com/api/v4/projects/53570758/releases/ \
            | jq -r '.[0].assets.links[] | select(.name=="release") | .url'
    )"

    if [ -z "$dist_url" ]; then
        echo 'Failed to get the latest release URL.'
        exit 1
    fi

    echo "Downloading $dist_url to $tmp_dir"
    curl -sL "$dist_url" | tar xz -C "$tmp_dir"

    echo "Installing to $install_location"
    mv "$tmp_dir"/dias "$install_location"
    chmod +x "$install_location/dias"

    return 0
}

rm -f "$user_install_location/dias" || echo ''

install_latest "$user_install_location"
