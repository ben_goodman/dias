use std::{collections::HashMap, fs, path::PathBuf};

use handlebars::Handlebars;
use log::debug;
use pathdiff::diff_paths;
use walkdir::{DirEntry, WalkDir};

#[derive(serde::Deserialize, Debug)]
struct TemplateConfig {
    display_name: String,
    inputs: Option<HashMap<String, InputConfig>>,
}

#[derive(serde::Deserialize, Debug, Clone)]
pub struct InputValidation {
    pub regex: String,
    pub error_message: String,
}

#[derive(serde::Deserialize, Debug, Clone)]
pub struct InputConfig {
    pub prompt: String,
    pub default: Option<String>,
    pub validation: Option<InputValidation>,
}

#[derive(Debug, Clone)]
pub struct Template {
    pub display_name: String,
    pub templates_directory: PathBuf,
    pub destination_directory: Option<PathBuf>,
    pub inputs: Option<HashMap<String, InputConfig>>,
    pub context: Option<HashMap<String, String>>,
}

impl Template {
    pub fn from_directory(
        templates_directory: PathBuf,
    ) -> Result<Template, Box<dyn std::error::Error>> {
        // read the json file templates_directory/dias.json
        let config_path = templates_directory.join("dias.json");
        let config_data = fs::read_to_string(&config_path)?;

        // custom error message for parsing the config file
        let config: TemplateConfig = match serde_json::from_str(&config_data) {
            Ok(config) => config,
            Err(e) => {
                eprintln!("Failed to parse {:?}\nReason: {}", config_path.display(), e);
                std::process::exit(1);
            }
        };

        debug!("Template config: {:?}", config);

        // destination_directory and context are set by the prompt generator
        Ok(Template {
            display_name: config.display_name,
            templates_directory,
            destination_directory: None,
            context: None,
            inputs: config.inputs,
        })
    }

    pub fn exec(&self) {
        // get .hbs files from the template directory
        let templates_dir = self.templates_directory.join("templates");

        // get all .hbs files in the templates directory
        let handlebar_files = WalkDir::new(&templates_dir)
            .into_iter()
            .filter_map(Result::ok)
            .filter(|entry| {
                entry.path().is_file()
                    && entry
                        .path()
                        .extension()
                        .unwrap_or_else(|| std::ffi::OsStr::new(""))
                        == "hbs"
            })
            .collect::<Vec<DirEntry>>();

        // map each template file to its destination file
        let template_file_tuples = handlebar_files.clone().into_iter().map(|file| {
            let source_path = file.clone().into_path();
            // get the source path relative to the templates directory
            let relative_template_path = diff_paths(&file.path(), &templates_dir).unwrap();
            let destination_path = self
                .destination_directory
                .clone()
                .unwrap()
                .join(relative_template_path)
                .with_extension("");

            (source_path, destination_path)
        });

        let handlebars = Handlebars::new();

        // for each .hbs file, render the template with the context
        for (source_path, destination_path) in template_file_tuples {
            // read the template file
            let template_file = fs::read_to_string(&source_path).unwrap();
            // render the template with the context
            let rendered_template = handlebars
                .render_template(&template_file, &self.context.as_ref().unwrap())
                .unwrap();

            // create the destination directory if it doesn't exist
            let destination_dir = destination_path.parent().unwrap();
            fs::create_dir_all(&destination_dir).unwrap();

            // write the rendered template to the destination file
            fs::write(&destination_path, rendered_template).unwrap();
        }
    }
}
