use env_logger::Env;
use log::debug;
use std::path::PathBuf;

use clap::Parser;
use home::home_dir;

mod prompt_generator;
mod template;
mod template_factory;

// the default templates dir is $HOME/.dias
fn templates_default_dir() -> Option<PathBuf> {
    let default_dir = home_dir().unwrap().join(".dias");
    // does the default directory exist?
    if default_dir.exists() {
        debug!("Found a default templates directory: {:?}", default_dir);
        return Some(default_dir);
    } else {
        None
    }
}

#[derive(Parser)]
#[command(version, about)]
struct CLIArgs {
    #[clap(long)]
    templates: Option<String>,
}

fn main() {
    // initialize the logger
    env_logger::Builder::from_env(Env::default().default_filter_or("info")).init();

    // the cli takes one optional argument, --templates.
    let cli_args = CLIArgs::parse();

    // if --templates is specified, check if it is a git repository
    let templates_url = match &cli_args.templates {
        Some(url) => match url.ends_with(".git") {
            true => Some(url),
            false => None,
        },
        None => None,
    };

    // if templates_url is Some, templates_directory is automatically None
    // else, check for --templates and use its value.
    // if --templates is not specified, use the default directory.
    // if the default directory does not exist, templates_directory is None.
    let templates_directory = match templates_url {
        Some(_) => None,
        // else,
        None => match &cli_args.templates {
            // if --templates is specified, use that directory
            Some(dir) => Some(PathBuf::from(dir)),
            // otherwise, use the default directory
            None => match templates_default_dir() {
                Some(dir) => Some(dir),
                None => None,
            },
        },
    };

    debug!("templates_url: {:?}", templates_url);
    debug!("templates_directory: {:?}", templates_directory);

    // if templates_url and templates_directory are both None, exit with an error
    if templates_url.is_none() && templates_directory.is_none() {
        println!("No templates directory or git repository specified. Exiting.");
        std::process::exit(1);
    }

    // create templates from the specified directory or git repository
    let templates = match templates_url.is_some() {
        true => template_factory::TemplateFactory::from_git_url(templates_url.unwrap().to_string()),
        false => template_factory::TemplateFactory::from_directory(templates_directory.unwrap()),
    };

    // unwrap the templates or exit with an error
    let templates = match templates {
        Ok(templates) => templates,
        Err(e) => {
            println!("{}", e);
            std::process::exit(1);
        }
    };

    // prompt the user to select a project template
    let selected_template = prompt_generator::PromptGenerator::project_selection_prompt(templates);

    // prompt the user to initialize the project
    // - project name, slug, destination, and any user-defined inputs.
    let initialized_template =
        prompt_generator::PromptGenerator::project_initialization_prompt(selected_template);

    // generate files from the template and save to the destination directory
    initialized_template.exec();

    println!(
        "Success! {:?}",
        initialized_template.destination_directory.unwrap()
    );
}
