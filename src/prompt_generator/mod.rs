use crate::template::Template;
use dialoguer::{Input, Select};
use home::home_dir;
use slugify::slugify;
use std::collections::HashMap;
use std::path::PathBuf;

pub struct PromptGenerator {}

impl PromptGenerator {
    pub fn project_selection_prompt(templates: Vec<Template>) -> Template {
        let selections = templates
            .iter()
            .map(|template| template.display_name.as_str())
            .collect::<Vec<&str>>();

        let selection = Select::new()
            .with_prompt("Select a project template")
            .items(&selections)
            .default(0)
            .interact()
            .unwrap();

        return templates[selection].clone();
    }

    pub fn project_initialization_prompt(mut template: Template) -> Template {
        let project_name = Input::<String>::new()
            .with_prompt("Enter a project name")
            .interact()
            .unwrap();

        // matches anything except lowercase letters, numbers, hyphens, and underscores
        let slug_validation_re = regex::Regex::new(r"^[a-z0-9-_]+$").expect("Invalid regex pattern.");
        let default_project_slug = slugify!(&project_name);

        let project_slug = Input::<String>::new()
            .with_prompt("Enter a project slug")
            .default(default_project_slug)
            .validate_with(|cli_input: &String| -> Result<(), &str> {
                if slug_validation_re.is_match(cli_input) {
                    Ok(())
                } else {
                    Err("Invalid project slug.  Use lowercase letters, numbers, hyphens, and underscores.")
                }
            })
            .interact()
            .unwrap();

        let project_destination = Input::<String>::new()
            .with_prompt("Enter a project destination")
            .default(
                home_dir()
                    .unwrap()
                    .join(project_slug.clone())
                    .to_str()
                    .unwrap()
                    .to_string(),
            )
            .interact()
            .unwrap();

        // use the template inputs config to generate contextual prompts
        let mut context_map = HashMap::new();

        // add the project name and slug to the context
        context_map.insert("project_name".to_string(), project_name.clone());
        context_map.insert("project_slug".to_string(), project_slug.clone());

        // generate prompts for each input in the template
        let context_input = match template.inputs.clone() {
            Some(inputs) => {
                for (key, input) in inputs.iter() {
                    let value = Input::<String>::new()
                        .with_prompt(input.prompt.as_str())
                        .show_default(input.default.is_some())
                        .default(input.default.clone().unwrap_or("".to_string()))
                        .validate_with(|cli_input: &String| -> Result<(), &str> {
                            // validate the input with the regex pattern
                            // if no pattern is provided, return Ok(())
                            match &input.validation {
                                Some(validation) => {
                                    let re = regex::Regex::new(&validation.regex)
                                        .expect("Invalid regex pattern.");
                                    if re.is_match(cli_input) {
                                        Ok(())
                                    } else {
                                        Err(validation.error_message.as_str())
                                    }
                                }
                                None => Ok(()),
                            }
                        })
                        .interact()
                        .unwrap();
                    context_map.insert(key.clone(), value);
                }
                context_map
            }
            None => context_map,
        };

        template.destination_directory = Some(PathBuf::from(project_destination));
        template.context = Some(context_input);

        return template;
    }
}
