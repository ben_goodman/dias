use crate::template::Template;
use log::debug;
use std::{
    env, fs, io,
    path::{Path, PathBuf},
    process::Command,
};
use tempdir::TempDir;

use path_clean::PathClean;

pub fn absolute_path(path: impl AsRef<Path>) -> io::Result<PathBuf> {
    let path = path.as_ref();

    let absolute_path = if path.is_absolute() {
        path.to_path_buf()
    } else {
        env::current_dir()?.join(path)
    }
    .clean();

    Ok(absolute_path)
}

type TemplateFactoryResult = Result<Vec<Template>, Box<dyn std::error::Error>>;

/**
 * The role of the TemplateFactory is to scan a location for project templates
 * and return a list of Template objects.
 */
pub struct TemplateFactory {}

impl TemplateFactory {
    pub fn from_git_url(url: String) -> TemplateFactoryResult {
        // clones a git repository to a temporary directory
        // runs Template::from_directory on the temporary directory
        // finally, returns the Template object
        let tmp_dir = TempDir::new("dias").unwrap().into_path();

        debug!(
            "Cloning git repository to temporary directory: {:?}",
            tmp_dir
        );

        Command::new("git")
            .arg("clone")
            .arg(url)
            .arg(tmp_dir.to_str().unwrap())
            .output()
            .expect("Failed to clone git repository.");

        let template = TemplateFactory::from_directory(tmp_dir).unwrap();

        return Ok(template);
    }

    pub fn from_directory(templates_directory: PathBuf) -> TemplateFactoryResult {
        let dir_path = Path::new(&templates_directory);
        let absolute_dir_path = absolute_path(dir_path).unwrap();

        debug!(
            "Scanning directory for project templates: {:?}",
            absolute_dir_path
        );

        // filter the content of absolute_dir_path for project templates
        // i.e., directories that have dias.json and templates directory.
        let directory_paths = fs::read_dir(absolute_dir_path);
        let template_directories = directory_paths
            .unwrap()
            .filter(|path| {
                let potential_template = path.as_ref().unwrap().path();
                let is_directory = potential_template.is_dir();

                if is_directory {
                    let dias_json_path = potential_template.join("dias.json");
                    let dias_json_exists = dias_json_path.exists();

                    if dias_json_exists {
                        let templates_path = potential_template.join("templates");
                        let templates_exists = templates_path.exists() && templates_path.is_dir();

                        return templates_exists;
                    } else {
                        return false;
                    }
                } else {
                    return false;
                }
            })
            .collect::<Vec<_>>();

        let mut templates = Vec::new();

        // error if no project templates are found
        if template_directories.is_empty() {
            return Err("No project templates found in the templates directory.  Exiting.".into());
        }

        template_directories.into_iter().for_each(|path| {
            let template_path = path.unwrap().path();
            let template = Template::from_directory(template_path).unwrap();
            templates.push(template);
        });

        return Ok(templates);
    }
}
