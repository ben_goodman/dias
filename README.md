# Dias

Scaffold new projects from a templates repository.

> (Linux only) Install the latest version: `brew install dias`

```bash
Usage: dias [OPTIONS]

Options:
      --templates <DIRECTORY OR REPO URL>
  -h, --help                           Print help
  -V, --version                        Print version
```

## Templates & Repositories

> Templates use the [Handlebars](https://handlebarsjs.com/) format.

A project template is a directory containing the following:

 - a directory named `templates/` - a collection of Handlebar files that can be used to scaffold a new project.
 - a `dias.json` config file.

A repository is a collection of project templates and can be a local directory or a git repo.

For example:

```text
~/.dias
├── README.md
├── example-websocket
│   ├── dias.json
│   └── templates
│       ├── Makefile.hbs
│       ├── ...
│       └── README.md.hbs
└── graphql-server
    ├── dias.json
    └── templates
        ├── Makefile.hbs
        ├── README.md.hbs
        ├── codegen.ts.hbs
        ├── eslint.config.mjs.hbs
        ├── package-lock.json.hbs
        ├── package.json.hbs
        └── src
            ├── generated
            │   └── graphql.ts.hbs
            ├── graph
            │   ├── resolvers.ts.hbs
            │   └── schema.ts.hbs
            ├── handlers
            │   └── createTodo.ts.hbs
            ...
            └── util
                └── isAuthenticated.ts.hbs
```

The location of the templates directory can be specified using the `--templates` option.

```bash
dias --templates ./path/to/templates

dias --templates https://gitlab.com/path/to/templates.git
```

If the `--templates` option is not provided, Dias will look for a `~/.dias` directory.  If a templates source cannot be found, Dias will exit with an error.

## dias.json

The `dias.json` file should contain the following properties:

 - `display_name` - The name of the template.
 - `inputs` - (Optional) An object containing custom variables that can be used in the template files.
    - `prompt` - The prompt to display to the user.
    - `default` - (Optional) The default value for the variable.
    - `validation` - (Optional) An object containing a regex pattern and an error message to validate the user input.

```json
{
  "display_name": "My Template",
  "inputs": {
    [variable_name]: {
        "prompt": "Prompt to display to the user",
        "default": "Optional default value",
        "validation": {
          "regex": "Regex pattern",
          "error_message": "Error message"
        }
    }
  }
}
```

## Template Variables

Dias has a set of default variables that can be used in the template files. These variables are derived from the default prompts:

| Prompt | Variable | Description |
|--------|----------|-------------|
| Project Name | `project_name` | A friendly name for the project. |
| Project Slug | `project_slug` | A URL-friendly version of the project name. |
| Project Directory | n/a | The directory to create the project in. |

Custom variables can be defined in the `dias.json` file.  For example, to prompt the user for the template variable `table_name`:

```json
  {
    ...
    "inputs": {
      "table_name": {
        "prompt": "Enter the name of the table",
        "default": "my_table"
      }
    }
  }
```

The user will be prompted to provide a value for each custom variable.  The value can be used in the template files using the variable name.