ENV_VARS_OUTPUT := ".PublishGeneric.env"
RUSTC_TARGET ?= "x86_64-unknown-linux-gnu"
PRE_RELEASE_TAG ?= ""

# CI vars fallback for testing locally
CI ?= false
CI_DEFAULT_BRANCH ?= main

PACKAGE_NAME := "$$(basename $$(cargo pkgid) | awk -F\# '{print $$1}')"
PACKAGE_VERSION_SHORT := "$$(basename $$(cargo pkgid) | awk -F\# '{print $$2}')"
PACKAGE_VERSION := "$(PACKAGE_VERSION_SHORT)$(PRE_RELEASE_TAG)"
PACKAGE_TAR := "$(PACKAGE_NAME)_$(PACKAGE_VERSION)_$(RUSTC_TARGET).tar.gz"

report_env:
	@echo $(CI_MERGE_REQUEST_IID)
	@echo $(PACKAGE_NAME)
	@echo $(PACKAGE_VERSION_SHORT)
	@echo $(PACKAGE_VERSION)
	@echo $(PACKAGE_TAR)
	@echo $(RUSTC_TARGET)

clean:
	rm -rf *.tar.gz
	rm -rf $(ENV_VARS_OUTPUT)

prepublish: clean
	@echo "PACKAGE_NAME: $(PACKAGE_NAME)"
	@echo "PACKAGE_VERSION: $(PACKAGE_VERSION)"
	@echo "PACKAGE_TAR: $(PACKAGE_TAR)"

# set cwd to the output dir and create & compress a new archive
	tar -C target/$(RUSTC_TARGET)/release -czvf "$(PACKAGE_TAR)" "$(PACKAGE_NAME)"
	@echo "Saving output to $(ENV_VARS_OUTPUT)"
	@echo "export PACKAGE_TAR=\"$(PACKAGE_TAR)\"" > $(ENV_VARS_OUTPUT)
	@echo "export PACKAGE_VERSION=\"$(PACKAGE_VERSION)\"" >> $(ENV_VARS_OUTPUT)


######################
## create git hooks
######################
define PRE_PUSH_HOOK_SCRIPT
#!/bin/bash
if ! cargo check; then
	echo "[Pre-push Hook 1]  Cargo check failed - see compiler output above."
	exit 1
fi

if ! cargo fmt --check; then
	echo "[Pre-push Hook 2] Code style issues found.  Run `cargo fmt --all` and commit your changes."
	exit 1
fi

if ! cargo test; then
	echo "[Pre-push Hook 3] One or more tests have failed.  Check the test output above."
	exit 1
fi

echo "Success!  All pre-push conditions met."
endef

export PRE_PUSH_HOOK_SCRIPT
HOOKS_DIR := "$$(git rev-parse --show-toplevel)/.git/hooks"

hooks:
	rm -rf "$(HOOKS_DIR)" || echo ""
	mkdir -p "$(HOOKS_DIR)" || echo ""
	git config --local core.hooksPath "$(HOOKS_DIR)"
	@echo "$$PRE_PUSH_HOOK_SCRIPT" > "$(HOOKS_DIR)/pre-push"
	chmod +x "$(HOOKS_DIR)/pre-push"
	./.git/hooks/pre-push
